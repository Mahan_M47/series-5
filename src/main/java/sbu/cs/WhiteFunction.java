package sbu.cs;

public interface WhiteFunction
{
    static String func(int n, String arg1, String arg2)
    {
        String result = "";
        switch (n) {
            case 1:
                result = func1(arg1, arg2);
                break;
            case 2:
                result = func2(arg1, arg2);
                break;
            case 3:
                result = func3(arg1, arg2);
                break;
            case 4:
                result = func4(arg1, arg2);
                break;
            case 5:
                result = func5(arg1, arg2);
                break;
        }
        return result;
    }

    static String func1(String arg1, String arg2)
    {
        StringBuilder str = new StringBuilder();

        int min = Math.min( arg1.length(), arg2.length() );
        int max = Math.max( arg1.length(), arg2.length() );

        for (int i = 0; i < min; i++) {
            str.append( arg1.charAt(i) ).append( arg2.charAt(i) );
        }

        if ( max == arg1.length() ) {
            str.append(arg1, min, max);
        }
        else str.append(arg2, min, max);

        return str.toString();
    }

    static String func2(String arg1, String arg2)
    {
        StringBuilder str = new StringBuilder();

        arg2 = str.append(arg2).reverse().toString();

        return arg1 + arg2;
    }

    static String func3(String arg1, String arg2)
    {
        StringBuilder str = new StringBuilder();
        arg2 = str.append(arg2).reverse().toString();

        return func1(arg1, arg2);
    }

    static String func4(String arg1, String arg2)
    {
        if (arg1.length() % 2 == 0) {
            return arg1;
        }
        else return arg2;
    }

    static String func5(String arg1, String arg2)
    {
        int min = Math.min( arg1.length(), arg2.length() );
        int max = Math.max( arg1.length(), arg2.length() );

        StringBuilder str = new StringBuilder();

        for (int i = 0; i < min; i++)
        {
            int num = (int) (arg1.charAt(i) - 97) + (arg2.charAt(i) - 97);
            num %= 26;
            char ch = (char) (num + 97);
            str.append(ch);
        }

        if ( max == arg1.length() ) {
            str.append(arg1, min, max);
        }
        else str.append(arg2, min, max);

        return str.toString();
    }

}
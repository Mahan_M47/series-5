package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input)
    {
        Machine process = new Machine(n, arr, input);

        Runnable_Vertical r1 = new Runnable_Vertical(process);
        Runnable_Horizontal r2 = new Runnable_Horizontal(process);

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String up = process.getUp(), left = process.getLeft();
        //up is the upper argument received by the the last WHITE function.
        //left is the left argument received by the the last WHITE function.

        return WhiteFunction.func( arr[n-1][n-1], left, up);
    }
}

package sbu.cs;

public interface BlackFunction
{
    static String func(int n, String arg)
    {
        String result = "";
        switch (n) {
            case 1:
                result = func1(arg);
                break;
            case 2:
                result = func2(arg);
                break;
            case 3:
                result = func3(arg);
                break;
            case 4:
                result = func4(arg);
                break;
            case 5:
                result = func5(arg);
                break;
        }
        return result;
    }

    static String func1(String arg)
    {
        StringBuilder str = new StringBuilder();
        return str.append(arg).reverse().toString();
    }

    static String func2(String arg)
    {
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < arg.length(); i++) {
            str.append( arg.charAt(i) ).append( arg.charAt(i) );
        }

        return str.toString();
    }

    static String func3(String arg)
    {
        return arg + arg;
    }

    static String func4(String arg)
    {
        int length = arg.length() - 1;
        StringBuilder str = new StringBuilder();
        str.append( arg.charAt(length) );

        for (int i = 0; i < length; i++) {
            str.append( arg.charAt(i) );
        }

        return str.toString();
    }

    static String func5(String arg)
    {
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < arg.length(); i++)
        {
            int num = (int) arg.charAt(i) - 96;
            num = 27 - num;
            char ch = (char) (num + 96);
            str.append(ch);
        }

        return str.toString();
    }

}

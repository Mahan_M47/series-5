package sbu.cs;

public class Runnable_Horizontal implements Runnable
{
    private final Machine process;

    public Runnable_Horizontal(Machine process) {
        this.process = process;
    }

    @Override
    public void run() {
        this.process.horizontal();
    }
}

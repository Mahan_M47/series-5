package sbu.cs;

public class Machine
{
    private        String up, left;
    private final  String input;
    private final  int[][] arr;
    private final  int n;

    public Machine(int n, int[][] arr, String input)
    {
        this.left = "";
        this.up = "";
        this.n = n;
        this.arr = arr;
        this.input = input;
    }

    public void vertical()
    {
        String s2, s1 = input;

        for (int i = 0; i < n - 1; i++)
        {
            s1 = BlackFunction.func(arr[i][0], s1);
            s2 = s1;

            for (int j = 1; j < n - 1; j++)
            {
                s2 = BlackFunction.func(arr[i][j], s2);

                if (i > 0 && j == n - 2) {
                    up = WhiteFunction.func(arr[i][j + 1], s2, up);
                }
                else if (i == 0 && j == n - 2) {
                    up = BlackFunction.func(arr[i][j + 1], s2);
                }
            }
        }

    }

    public void horizontal()
    {
        String s2, s1 = input;

        for (int j = 0; j < n - 1; j++)
        {
            s1 = BlackFunction.func(arr[0][j], s1);
            s2 = s1;

            for (int i = 1; i < n - 1; i++)
            {
                s2 = BlackFunction.func(arr[i][j], s2);

                if (j > 0 && i == n - 2) {
                    left = WhiteFunction.func(arr[i + 1][j], left, s2);
                }
                else if (j == 0 && i == n - 2) {
                    left = BlackFunction.func(arr[i + 1][j], s2);
                }
            }
        }

    }

    public String getLeft() {
        return left;
    }

    public String getUp() {
        return up;
    }
}

package sbu.cs;

public class SortArray {

    public void swap (int[] arr, int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size)
    {
        for (int i = 0; i < size; i++) {
            int index_min = i;

            for (int j = i + 1; j < size; j++) {
                if (arr[j] < arr[index_min]) {
                    index_min = j;
                }
            }

            swap(arr, i, index_min);
        }

        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size)
    {
        for (int i = 1; i < size; i++){
            int j, key = arr[i];

            for (j = i - 1; j >= 0; j--){
                if (arr[j] > key){
                    arr[j + 1] = arr[j];
                }
                else break;
            }

            arr[j + 1] = key;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size)
    {
        Sort(arr, size);
        return arr;
    }

    public void Sort(int[] arr, int size)
    {
        if (size <= 1) { return; }

        int[] first  = new int[size / 2];
        int[] second = new int[size - first.length];

        for (int i = 0; i < first.length; i++) {
            first[i] = arr[i];
        }

        for (int i = 0; i < second.length; i++) {
            second[i] = arr[first.length + i];
        }

        Sort(first, first.length);
        Sort(second, second.length);

        Merge(first, second, arr);
    }

    public void Merge(int[] first, int[] second, int[] arr)
    {
        int i = 0, j = 0, k = 0;

        while (i < first.length && j < second.length)
        {
            if (first[i] < second[j]) {
                arr[k] = first[i];
                i++;
            }
            else {
                arr[k] = second[j];
                j++;
            }
            k++;
        }

        while (i < first.length) {
            arr[k] = first[i];
            i++;
            k++;
        }
        while (j < second.length) {
            arr[k] = second[j];
            j++;
            k++;
        }

    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value)
    {
        int size = arr.length;
        int x = (size - 1) / 4;
        int mid = (size - 1) / 2;

        while (true)
        {
            if (arr[mid] == value) {
                return mid;
            }
            else if (arr[mid] < value) {
                mid += x;
            }
            else if (arr[mid] > value) {
                mid -= x;
            }

            if (x == 0)
            {
                if (arr[mid - 1] == value) {
                    return mid - 1;
                }
                else if (arr[mid + 1] == value) {
                    return mid + 1;
                }
                else if (mid + 2 < size && arr[mid + 2] == value) {
                    return mid + 2;
                }
                else break;
            }

            x /= 2;
        }

        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value)
    {
        return binarySearchRecursive(arr, value, 0, arr.length - 1);
    }

    public int binarySearchRecursive(int[] arr, int value, int start, int end)
    {
        int middle = (start + end) / 2;

        if (end < start) {
            return -1;
        }

        else if (value == arr[middle]) {
            return middle;
        }

        else if (value < arr[middle]) {
            return binarySearchRecursive(arr, value, start, middle - 1);
        }

        else if (value > arr[middle]) {
            return binarySearchRecursive(arr, value, middle + 1, end);
        }

        else return -1;
    }
}

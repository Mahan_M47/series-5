package sbu.cs;

public class Runnable_Vertical implements Runnable
{
    private final Machine process;

    public Runnable_Vertical(Machine process) {
        this.process = process;
    }

    @Override
    public void run() {
        this.process.vertical();
    }
}
